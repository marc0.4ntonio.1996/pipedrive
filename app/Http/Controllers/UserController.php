<?php

namespace App\Http\Controllers;

use App\Models\User;
use Devio\Pipedrive\Pipedrive;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $token = config('services.pipedrive.token');
        $pipedrive = new Pipedrive($token);

        

        // Organización
        $organization = $pipedrive->organizations()->add(['name' => $request['company_name']]);
        $orgId = $organization->getData()->id;

        // Cliente
        $client = $pipedrive->persons()->add([
            'name' => $request['name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'org_id' => $orgId,
        ]);
        $clientId = $client->getData()->id;


        $users = $pipedrive->users()->all(); // agentes de ventas en pipedirve

        $admins = $users->getData();

        //Busca el email del admin al que pertenece el cliente en axincapital y lo asigna a ese mismo en pipedrive

        foreach($admins as $admin) {
            if($admin->email == 'judith@axincapital.com') {
                
                // Negocio
                $deal = $pipedrive->deals()->add([
                    'title' => $request['company_name'],
                    'person_id' => $clientId,
                    'user_id' => $admin->id,
                    
                ]);

                break;
            }
          
        }

        // // Notas
        // $notes = $pipedrive->notes()->add([
        //     'content' => $request['message'],
        //     'deal_id' => $dealId,
        // ]);

        // Axin Capital
        //$contact = User::create($request->all());
        return redirect()->back()->with('info', 'usuario guardo en pipedrive');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        //actualizar datos
        //$pipedrive->persons->update(1, ['name' => 'Israel Ortuno']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
