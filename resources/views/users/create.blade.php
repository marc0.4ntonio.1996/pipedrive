<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div class="card">
        <div class="card-body">
            {!! Form::open(['route' => 'users.store']) !!}
            
            <div class="form-group">
                {!! Form::label('name', 'Compañia') !!}
                {!! Form::text('company_name', null, ['class' => 'form-control'.($errors->has('company_name') ? ' is-invalid': '')]) !!}
                @error('company_name')
                    <span class="invalid-feedback">
                        {{ $message }}
                    </span>
                @enderror
            </div>
            
            <div class="form-group">
                {!! Form::label('name', 'Nombre') !!}
                {!! Form::text('name', null, ['class' => 'form-control'.($errors->has('name') ? ' is-invalid': '')]) !!}
                @error('name')
                    <span class="invalid-feedback">
                        {{ $message }}
                    </span>
                @enderror
            </div>
            
            <div class="form-group">
                {!! Form::label('name', 'Correo') !!}
                {!! Form::email('email', null, ['class' => 'form-control'.($errors->has('email') ? ' is-invalid': '')]) !!}
                @error('email')
                    <span class="invalid-feedback">
                        {{ $message }}
                    </span>
                @enderror
            </div>
            
            <div class="form-group">
                {!! Form::label('name', 'Telefono') !!}
                {!! Form::number('phone', null, ['class' => 'form-control'.($errors->has('phone') ? ' is-invalid': '')]) !!}
                @error('phone')
                    <span class="invalid-feedback">
                        {{ $message }}
                    </span>
                @enderror
            </div>
            
            <div class="form-group">
                {!! Form::label('name', 'Mensaje') !!}
                {!! Form::textarea('message', null, ['class' => 'form-control'.($errors->has('message') ? ' is-invalid': '')]) !!}
                @error('message')
                    <span class="invalid-feedback">
                        {{ $message }}
                    </span>
                @enderror
            </div>

            <br>
            {!! Form::submit('Crear', ['class'=>'btn btn-primary mt-2']) !!}
            {!! Form::close() !!}
        </div>
    </div>
  
</body>
</html>