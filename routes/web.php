<?php

use App\Http\Controllers\UserController;
use Devio\Pipedrive\Pipedrive;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   
    $pipedrive = new Pipedrive(config('services.pipedrive.token'));
    $organizations = $pipedrive->organizations()->all();
    $users = $pipedrive->users()->all();
    $deals = $pipedrive->deals()->all();
    dd($users);
    
    return $pipedrive;
});

Route::resource('users', UserController::class)->names('users');;
